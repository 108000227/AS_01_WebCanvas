# Software Studio 2021 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%     | N         |


---

### How to use 

    Describe how to use your web and maybe insert images to help you explain.

### Function description

    init():
        load canvas and add addEventListener for events
    
    getxy():
        specify which mouse event occurs and record the coordinate of previous mouse and current mouse.
    
    draw_dot():
    draw_path():
        for tool pencil, draw_dot() is the for mousedown event and draw_path() is for the mousemove event.
    
    erase_dot():
    erase_path():
        for tool eraser, erase_dot() is for the mousedown event and erase_path() is the for mousemove event.

    draw_circle():
    finish_circle():
        for tool circle, since I use two canvas for drawing a shape, when I draw a shape, I let the last image to show on the second canvas, and after drawing a shape, I let the image back to show on the first canvas. So draw_circle() is for the mousedown event and finish_circle() is for the mousemove event.
    
    draw_rect():
    finish_rect():
        for tool rectangle, draw_rect() is for the mousedown event and finish_rect() is for the mousemove event.
    
    draw_tri():
    finish_tri():
        for tool triangle, draw_tri() is for the mousedown event and finish_tri() is for the mousemove event.
    
    show_block():
        for tool text, when click on the canvas, the text box will appear according the mousedown coordinate for users to type text .

    type():
        for tool text, after typing text and press "enter", the text will show on the canvas according the mousedown coordinate.
    
    push():
        for tool undo/redo, after each step(mouseup event), the current image will be stored in a stack.
    
    undo():
        for tool undo, the canvas will show the previous imgae relative to current image stored in stack.
    
    redo():
        for tool redo, the canvas will show the next imgae relative to current image stored in stack.
    
    handleImage():
        for tool upload, users can choose only image files to upload on the canvas.
    
    change_tool():
        show the border of a tool to specify which tool is used now and change the cursor icon according to current used tool.
    
    change_color():
        get the value of input color and change the pencil/text/shape color.
    
    change_width():
        get the value of input range and change the pencil/shape width.
    
    change_font():
    change_font_size():
        get the value of select and change the text font/font_size.
    
    refresh():
        for tool refresh, to clear the canvas.


### Gitlab page link

    your web page URL, which should be "https://[studentID].gitlab.io/AS_01_WebCanvas"

### Others (Optional)

    Anythinh you want to say to TAs.

<style>
table th{
    width: 100%;
}
</style>