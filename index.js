var canvas, canvas2, ctx, ctx2, imageLoader;
var preX=0, curX=0, preY=0, curY=0;
var pre_act, cur_act="pencil";
var flag=false;

var width = 3, font="Times New Roman", font_size=12;
var color = "black";

var mouseX=0, mouseY=0;
var startX=0, startY=0;

var arr = new Array();
var idx = -1;

window.onload = function() {
    canvas = document.getElementById("myCanvas");
    canvas2 = document.getElementById("shapeCanvas");
    imageLoader = document.getElementById("upload");
    init(); 
}

function init() { 
    if(canvas.getContext) {
        console.log("getContext successfully.")
        ctx = canvas.getContext("2d"); 
    }
    else {
        console.log("getContext failed");
    }
    
    if(canvas2.getContext) {
        console.log("getContext2 successfully.")
        ctx2 = canvas2.getContext("2d");
    }
    else {
        console.log("getContext2 failed");
    }
    
    /*
    addEventListener():
    sets up a function that will be called whenever the specified event is delivered to the target.
    */
    canvas.addEventListener("mousemove", function(e) {
        getxy("move", e);
    }, false);
    canvas.addEventListener("mousedown", function(e) {
        getxy("down", e);
    }, false);
    canvas.addEventListener("mouseup", function(e) {
        getxy("up", e);
    }, false);
    canvas.addEventListener("mouseout", function(e) {
        getxy("out", e);
    }, false);
    canvas.addEventListener("click", function(e) {
        if(cur_act == "text")
            show_block(e);
    }, false);
    document.addEventListener("keydown", function(e) { // add keydown event to document
        if(cur_act == "text") {
            type(e);
        }       
    }, false)
    imageLoader.addEventListener("change", handleImage, false);
    push();
}

function draw_dot() {
    ctx.beginPath();
    ctx.arc(curX, curY, width/1.9, 0, Math.PI*2); //arc(x, y, r, startangle, endangle)
    ctx.fillStyle = color;
    ctx.fill();
    ctx.closePath();
}

function draw_path() {
    ctx.beginPath();
    ctx.lineCap = "round";
    ctx.moveTo(preX, preY);
    ctx.lineTo(curX, curY);
    ctx.strokeStyle = color;
    ctx.lineWidth = width;
    ctx.stroke();
    ctx.closePath();
}

function erase_dot() {
    ctx.beginPath();
    ctx.clearRect(curX, curY, width*2.5, width*2.5);
    ctx.closePath();
}

function erase_path() {
    ctx.beginPath();
    ctx.moveTo(preX, preY);
    ctx.lineTo(curX, curY);
    ctx.clearRect(curX, curY, width*2.5, width*2.5);
    ctx.closePath();
}

function getxy(res, e) {
    if(res == "down") {
        preX = curX;
        preY = curY;
        /*
        MouseEvent.clientX:
        read-only property of the MouseEvent interface provides the horizontal coordinate
        object.offsetLeft:
        Return the left offset position
        */
        curX = e.clientX - canvas.offsetLeft;
        curY = e.clientY - canvas.offsetTop;

        flag = true;
        switch(cur_act) {           
            case "pencil":
                draw_dot();
                break;
            case "eraser":
                erase_dot();
                break;
            case "circle":
            case "rectangle":
            case "triangle":
                startX = curX;
                startY = curY;
                ctx2.putImageData(arr[idx], 0, 0);   
                ctx.clearRect(0, 0, canvas.width, canvas.height);                      
                break;
            default:
                console.log("undefined behavior");
        }     
    }
    else if(res == "up") {
        flag = false;      
        switch(cur_act) {
            case "circle":
                finish_circle();
                break;
            case "rectangle":
                finish_rect();
                break;            
            case "triangle":
                finish_tri();
                break;            
        }

        if(cur_act != "text")
            push();       
    }
    else if(res == "out") {
        flag = false;
    }
    else if(res == "move") {
        if(flag) {
            preX = curX;
            preY = curY;
            curX = e.clientX - canvas.offsetLeft;
            curY = e.clientY - canvas.offsetTop;
            switch(cur_act) {
                case "pencil":
                    draw_path();
                    break;
                case "eraser":
                    erase_path();
                    break;               
                case "circle":
                    draw_circle();
                    break;
                case "rectangle":
                    draw_rect();
                    break;               
                case "triangle":
                    draw_tri();
                    break;
                default:
                    console.log("undefined behavior");
            }          
        }
    }
}

function draw_circle() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.beginPath();    
    // start point
    ctx.moveTo(startX, startY + (curY-startY)/2);
    // (first Bézier control point), (second Bézier control point), (ending point)
    ctx.bezierCurveTo(startX, startY, curX, startY, curX, startY + (curY-startY)/2);
    ctx.bezierCurveTo(curX, curY, startX, curY, startX, startY + (curY-startY)/2);
    ctx.closePath();
    ctx.stroke();
}

function finish_circle() {
    ctx.putImageData(arr[idx], 0, 0);
    ctx2.clearRect(0, 0, canvas2.width, canvas2.height);

    ctx.beginPath();
    ctx.moveTo(startX, startY + (curY-startY)/2);
    ctx.bezierCurveTo(startX, startY, curX, startY, curX, startY + (curY-startY)/2);
    ctx.bezierCurveTo(curX, curY, startX, curY, startX, startY + (curY-startY)/2);
    ctx.closePath();
    ctx.stroke();
}

function draw_rect() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.beginPath();
    ctx.strokeRect(startX, startY, curX-startX, curY-startY);
    ctx.closePath();
}

function finish_rect() {
    ctx.putImageData(arr[idx], 0, 0);
    ctx2.clearRect(0, 0, canvas.width, canvas.height);

    ctx.beginPath();  
    ctx.strokeRect(startX, startY, curX-startX, curY-startY);
    ctx.closePath();
}

function draw_tri() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.beginPath();   
    ctx.moveTo(startX, startY);
    ctx.lineTo(curX, curY);
    ctx.lineTo(startX + (startX-curX), curY);
    ctx.closePath();
    ctx.stroke();
}

function finish_tri() {
    ctx.putImageData(arr[idx], 0, 0);
    ctx2.clearRect(0, 0, canvas2.width, canvas2.height);

    ctx.beginPath();
    ctx.moveTo(startX, startY);
    ctx.lineTo(curX, curY);
    ctx.lineTo(startX + (startX-curX), curY);
    ctx.closePath();
    ctx.stroke();
}

function show_block(e) {
    mouseX = e.clientX - canvas.offsetLeft;
    mouseY = e.clientY - canvas.offsetTop;
    var pos = document.getElementById("block");
    pos.type = "text";
    pos.style.left =  mouseX + "px";
    pos.style.top = mouseY + "px";
}

function type(e) {
    var pos = document.getElementById("block");

    ctx.fillStyle = color;
    ctx.font = font_size + "px " + font;
    if(e.key == "Enter") {
        ctx.fillText(pos.value, mouseX, mouseY);
        pos.type = "hidden";
        pos.value = "";
        push();
    }
}

function push() {
    idx++;
    if(idx < arr.length) arr.length = idx;
    arr.push(ctx.getImageData(0, 0, canvas.width, canvas.height));
}

function undo() {
    if(idx > 0) {
        idx--;
        ctx.putImageData(arr[idx], 0, 0);
    }
}

function redo() {
    if(idx < arr.length-1) {
        idx++;
        ctx.putImageData(arr[idx], 0, 0);
    }
}

function handleImage(e) {
    var reader = new FileReader();
    reader.onload = function(event){
        var img = new Image();
        img.onload = function(){
            ctx.drawImage(img, 0, 0);
        }
        img.src = event.target.result;
    }
    reader.readAsDataURL(e.target.files[0]); 
}

function change_tool(tool) {
    pre_act = cur_act;
    cur_act = tool;
    document.getElementById(pre_act).style.border = "";
    document.getElementById(cur_act).style.border = "1px solid black";

    if(tool == "pencil")
        canvas.style.cursor = "url(img/pencil_icon.png) 20 26, auto"; // view x, y as center
    else if(tool == "eraser")
        canvas.style.cursor = "url(img/eraser_icon.png) 5 18, auto";
    else if(tool == "text")
        canvas.style.cursor = "text";
    else if(tool == "circle" || tool == "rectangle" || tool == "triangle")
        canvas.style.cursor = "url(img/" + tool + "_icon.png) 0 0, auto";
    else if(tool == "undo") {
        canvas.style.cursor = "auto";
        undo();
    }   
    else if(tool == "redo") {
        canvas.style.cursor = "auto";
        redo();
    }
    else {
        canvas.style.cursor = "auto";
    }     
}

function change_color(val) {
    color = val;
    ctx.fillStyle = val;
    ctx.strokeStyle = val;
}

function change_width(val) {
    width = val / 2.0;
    ctx.lineWidth = width;
    ctx.strokeWidth = width;
}

function change_font(val) {
    font = val;
}

function change_font_size(val) {
    font_size = val;
}

function refresh() {
    if(confirm("want to clear?")) {
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        push();
    }
}
